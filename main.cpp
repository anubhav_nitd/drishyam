#include <unistd.h>
#include <wiringPi.h>
#include <string>
#include <csignal>
#include <opencv2/opencv.hpp>
#include "utils.h"

/* To drive a motor forward, make F pin positive with respect to B pin.
 * Do the reverse to drive the motor backward.
 */
#define LF 6             // WiringPi pin 6 (physical 22) as left forward.
#define LB 10            // WiringPi pin 10 (physical 24) as left backward.
#define RF 11            // WiringPi pin 11 (physical 26) as right forward.
#define RB 13            // WiringPi pin 13 (physical 28) as right backward.

const std::string moduleName             = "Main";
bool              terminateRequested     = false;

void intHandler(int);

int main() {
  signal(SIGINT, intHandler);
  cv::VideoCapture frameFetch(0);                          // open the stream to RPi camera

  if(!frameFetch.isOpened()) {
    Log::log(moduleName, Log::level::FAILURE, std::string("Cannot access camera. Exiting . . .\n"));
    return 1;
  }

/*
  if(!v.set(cv::CAP_PROP_FRAME_WIDTH, 2592)) {
    log("Could not set frame width. Exiting . . .\n");
    return 1;
  }

  if(!v.set(cv::CAP_PROP_FRAME_HEIGHT, 1944)) {
    log("Could not set frame height. Exiting . . .\n");
    return 1;
  }
*/

  cv::Mat image;

  for(int i = 1; !terminateRequested; i++) {
    frameFetch >> image;
    cv::imwrite(std::to_string(i) + ".jpg", image);
    sleep(5); //cv::waitKey(5000);                                       // take a photo every 5 seconds
  }

  Log::log(moduleName, Log::level::ASSERT, std::string("Program terminated"));
  return 0;
}

void intHandler(int) {
  terminateRequested = true;
}
