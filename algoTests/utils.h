#ifndef UTILS_H
#define UTILS_H

#include <string>

namespace Log {
  enum level {
    ASSERT   = 0,
    WARN     = 1,
    ERROR    = 2,
    FAILURE  = 3
  };

  const std::string logLevels[] = {"ASSERT", "WARN", "ERROR", "FAILURE"};

  void log(const std::string &moduleName, level l, const std::string &message);
}; // namespace log

#endif // UTILS_H
