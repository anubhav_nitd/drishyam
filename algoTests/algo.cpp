#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include <unistd.h>
#include <cmath>
#include <csignal>
#include <sstream>
#include "utils.h"

const int ROWU = 154, ROWD = 324, COLL = 191, COLR = 448, ROWC = 240,  COLC = 320, THRESHOLD= 20;
const std::string moduleName("Algo");
bool sigint = false;

bool similar(const cv::Vec3b &v1, const cv::Vec3b &v2);
bool similar(const cv::Vec3b &v1, const cv::Vec3b &v2, const cv::Vec3b &v3);
bool grey(const cv::Vec3b &v);
void sigintHandler(int);

int main(int argc, char **argv) {
  signal(SIGINT, sigintHandler);
  cv::VideoCapture capture(0);

  cv::Mat frame, l, r;
  cv::Mat lmask = cv::imread("leftmask.png", cv::IMREAD_COLOR);
  cv::Mat rmask = cv::imread("rightmask.png", cv::IMREAD_COLOR);

  cv::Vec3b upoint, dpoint, mpoint;

  bool detected = false;
  double elapsed;
  std::stringstream message;

  while(!detected) {
    elapsed = (double) cv::getTickCount();
    capture >> frame;
    mpoint = frame.at<cv::Vec3b>(ROWC, COLC);

    upoint = frame.at<cv::Vec3b>(ROWU, COLL); dpoint = frame.at<cv::Vec3b>(ROWD, COLL);
    if(!grey(upoint) && !grey(dpoint) && similar(upoint, dpoint, mpoint)) {
      message.str(std::string());
      l = frame & lmask;
      cv::imwrite("detected.jpg", l);
      cv::imwrite("original.jpg", frame);
      message << "Left arrow detected. UPoint = " << upoint << ", DPoint = " << dpoint << ", MPoint = " << mpoint << '.';
      std::cout << message.str() << std::endl;
      Log::log(moduleName, Log::level::ASSERT, message.str());
      break;
    }

    upoint = frame.at<cv::Vec3b>(ROWU, COLR); dpoint = frame.at<cv::Vec3b>(ROWD, COLR);
    if(!grey(upoint) && !grey(dpoint) && similar(upoint, dpoint, mpoint)) {
      message.str(std::string());
      r = frame & rmask;
      cv::imwrite("detected.jpg", r);
      cv::imwrite("original.jpg", frame);
      message << "Right arrow detected. UPoint = " << upoint << ", DPoint = " << dpoint << ", MPoint = " << mpoint << '.';
      std::cout << message.str() << std::endl;
      Log::log(moduleName, Log::level::ASSERT, message.str());
      break;
    }

    if(sigint) {
      Log::log(moduleName, Log::level::ASSERT, "Program terminated.");
      break;
    }
    elapsed = ((double) cv::getTickCount() - elapsed) / (double) cv::getTickFrequency();
    Log::log(moduleName, Log::level::ASSERT, "Loop took " + std::to_string(elapsed) + " seconds.");
  }

  capture.release();
  return 0;
}

bool similar(const cv::Vec3b &v1, const cv::Vec3b &v2) {
  cv::Vec3b result;
  result[0] = abs(v1[0] - v2[0]);
  result[1] = abs(v1[1] - v2[1]);
  result[2] = abs(v1[2] - v2[2]);
  return ((result[0] <= THRESHOLD) && (result[1] <= THRESHOLD) && (result[2] <= THRESHOLD));
}

bool similar(const cv::Vec3b &v1, const cv::Vec3b &v2, const cv::Vec3b &v3) {
  return (similar(v1, v2) && similar(v2, v3) && similar(v1, v3));
}

bool grey(const cv::Vec3b &v) {
  return ((abs(v[0]-v[1]) <= THRESHOLD) && (abs(v[1]-v[2]) <= THRESHOLD) && (abs(v[2]-v[0]) <= THRESHOLD));
}

void sigintHandler(int) {
  sigint = true;
}
