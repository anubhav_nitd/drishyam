#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include <unistd.h>
#include <cmath>
#include <csignal>
#include <sstream>
#include <wiringPi.h>
#include "utils.h"

/* To drive a motor forward, make F pin positive with respect to B pin.
 * Do the reverse to drive the motor backward.
 */
#define LF 6             // WiringPi pin 6 (physical 22) as left forward.
#define LB 10            // WiringPi pin 10 (physical 24) as left backward.
#define RF 11            // WiringPi pin 11 (physical 26) as right forward.
#define RB 13            // WiringPi pin 13 (physical 28) as right backward.

const int ROWU = 154, ROWD = 324, COLL = 191, COLR = 448, ROWC = 240,  COLC = 320, THRESHOLD= 20;
const int TIMETO90 = 500;           // time in milliseconds
const std::string moduleName("Main");
bool sigint = false;

bool similar(const cv::Vec3b &v1, const cv::Vec3b &v2);
bool similar(const cv::Vec3b &v1, const cv::Vec3b &v2, const cv::Vec3b &v3);
bool grey(const cv::Vec3b &v);
void sigintHandler(int);

void initOuts();
void forw();
void back();
void left();
void right();
void stop();

int main(int argc, char **argv) {
  signal(SIGINT, sigintHandler);
  wiringPiSetup();
  initOuts();
  cv::VideoCapture capture(0);

  cv::Mat frame, l, r;
  cv::Mat lmask = cv::imread("leftmask.png", cv::IMREAD_COLOR);
  cv::Mat rmask = cv::imread("rightmask.png", cv::IMREAD_COLOR);

  cv::Vec3b lupoint, ldpoint, rupoint, rdpoint, cupoint, cdpoint, mpoint;

  bool detected = false;
  double elapsed;
  std::stringstream message;

  while(!detected) {
    forw();
    elapsed = (double) cv::getTickCount();
    capture >> frame;
    mpoint  = frame.at<cv::Vec3b>(ROWC, COLC);
    cupoint = frame.at<cv::Vec3b>(ROWU, COLC);
    cdpoint = frame.at<cv::Vec3b>(ROWD, COLC);

    while(similar(cupoint, mpoint, cdpoint) && !sigint) {
      Log::log(moduleName, Log::level::WARN, "Arrow is too close to the camera.");
      std::cout << "Arrow is too close to the camera." << std::endl;
      back();
      delay(400);
      stop();
      capture >> frame;
      mpoint  = frame.at<cv::Vec3b>(ROWC, COLC);
      cupoint = frame.at<cv::Vec3b>(ROWU, COLC);
      cdpoint = frame.at<cv::Vec3b>(ROWD, COLC);
    }
    if(sigint) {
      Log::log(moduleName, Log::level::ASSERT, "Pogram terminated.");
      break;
    }

    lupoint = frame.at<cv::Vec3b>(ROWU, COLL); ldpoint = frame.at<cv::Vec3b>(ROWD, COLL);
    if(!grey(lupoint) && !grey(ldpoint) && similar(lupoint, ldpoint, mpoint)) {
      message.str(std::string());
      l = frame & lmask;
      cv::imwrite("detected.jpg", l);
      cv::imwrite("original.jpg", frame);
      message << "Left arrow detected. UPoint = " << lupoint << ", DPoint = " << ldpoint << ", MPoint = " << mpoint << '.';
      std::cout << message.str() << std::endl;
      Log::log(moduleName, Log::level::ASSERT, message.str());
      left();
      delay(TIMETO90);
    }

    rupoint = frame.at<cv::Vec3b>(ROWU, COLR); rdpoint = frame.at<cv::Vec3b>(ROWD, COLR);
    if(!grey(rupoint) && !grey(rdpoint) && similar(rupoint, rdpoint, mpoint)) {
      message.str(std::string());
      r = frame & rmask;
      cv::imwrite("detected.jpg", r);
      cv::imwrite("original.jpg", frame);
      message << "Right arrow detected. UPoint = " << rupoint << ", DPoint = " << rdpoint << ", MPoint = " << mpoint << '.';
      std::cout << message.str() << std::endl;
      Log::log(moduleName, Log::level::ASSERT, message.str());
      right();
      delay(TIMETO90);
    }

    if(sigint) {
      Log::log(moduleName, Log::level::ASSERT, "Program terminated.");
      break;
    }
    elapsed = ((double) cv::getTickCount() - elapsed) / (double) cv::getTickFrequency();
    Log::log(moduleName, Log::level::ASSERT, "Loop took " + std::to_string(elapsed) + " seconds.");
  }

  capture.release();
  return 0;
}

bool similar(const cv::Vec3b &v1, const cv::Vec3b &v2) {
  cv::Vec3b result;
  result[0] = abs(v1[0] - v2[0]);
  result[1] = abs(v1[1] - v2[1]);
  result[2] = abs(v1[2] - v2[2]);
  return ((result[0] <= THRESHOLD) && (result[1] <= THRESHOLD) && (result[2] <= THRESHOLD));
}

bool similar(const cv::Vec3b &v1, const cv::Vec3b &v2, const cv::Vec3b &v3) {
  return (similar(v1, v2) && similar(v2, v3) && similar(v1, v3));
}

bool grey(const cv::Vec3b &v) {
  return ((abs(v[0]-v[1]) <= THRESHOLD) && (abs(v[1]-v[2]) <= THRESHOLD) && (abs(v[2]-v[0]) <= THRESHOLD));
}

void sigintHandler(int) {
  sigint = true;
}

void initOuts() {
  pinMode(LF, OUTPUT);
  pinMode(LB, OUTPUT);
  pinMode(RF, OUTPUT);
  pinMode(RB, OUTPUT);
}

void forw() {
  stop();
  digitalWrite(LF, HIGH);
  digitalWrite(RF, HIGH);
}

void back() {
  stop();
  digitalWrite(LB, HIGH);
  digitalWrite(RB, HIGH);
}

void left() {
  stop();
  digitalWrite(RF, HIGH);
  digitalWrite(LB, HIGH);
}

void right() {
  stop();
  digitalWrite(LF, HIGH);
  digitalWrite(RB, HIGH);
}

void stop() {
  digitalWrite(LF, LOW);
  digitalWrite(RF, LOW);
  digitalWrite(LB, LOW);
  digitalWrite(RB, LOW);
}
