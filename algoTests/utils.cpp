#include <fstream>
#include <ctime>
#include <iomanip>
#include <string>

#include "utils.h"

void Log::log(const std::string &moduleName, Log::level l, const std::string &message) {
  static std::fstream     Logger("algoLogs.log", std::ios::app);
  static std::time_t      currentTime;
  static char             arrTime[40];

  currentTime = std::time(0);
  std::strftime(arrTime, sizeof(arrTime), "%c", std::localtime(&currentTime));

  Logger << arrTime << ": " << moduleName << " ["
         << logLevels[l] << "] " << message << std::endl;
} // log()
